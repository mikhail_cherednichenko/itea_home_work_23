const comments = document.getElementById("commentsBlock");
const [...btn] = document.getElementsByClassName("comments__make-comment");
const commentsNum = document.getElementById("commentsNum");
const loader = document.getElementById("loader");
const comCount = 10;
let arr = [];

btn.forEach(el => {
    el.addEventListener("click", () => {

        loaderOn();

        const xhr = new XMLHttpRequest();
        // Создание объекта для HTTP запроса.
        xhr.open("GET", "https://jsonplaceholder.typicode.com/comments/");  // Настройка объекта для отправки асинхронного GET запроса
        
        // функция-обработчик срабатывает при изменении свойства readyState
        // Значения свойства readyState:
        // 0 - Метод open() еще не вызывался
        // 1 - Метод open() уже был вызван, но метод send() еще не вызывался.
        // 2 - Метод send() был вызван, но ответ от сервера еще не получен
        // 3 - Идет прием данных от сервера. Для значения 3 Firefox вызывает обработчик события несколько раз IE только один раз.
        // 4 - Ответ от сервера полностью получен (Запрос успешно завершен).

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) { // если получен ответ
                if (xhr.status == 200) { // и если статус код ответа 200
                    arr = JSON.parse(xhr.responseText);
                    // responseText - текст ответа полученного с сервера.
                    commentsNum.innerHTML = arr.length;
                    showComment(1);
                    showPaggination(arr, 1);
                    loaderOff();    
                } else {
                    console.log("error");
                };
            };
        };

        xhr.send();

        // Отправка запроса, так как запрос асинхронный сценарий продолжит свое выполнение. Когда с сервера придет ответ сработает событие onreadystatechange
    });
});

function showComment(num) {
    let count = num * comCount;
    let start = (num -1)* comCount;
    let com = "";

    comments.innerHTML = "";

    for (i = start; i < count; i++) {
        com = `
        <div class="comments__comment-box">
            <div class="comments__comment-title">
                <h2 class="comments__user"><span class="comments__id">${arr[i].id}. </span>${arr[i].name}</h2>
                <span class="comments__mail">${arr[i].email}</span>
            </div>
            <div class="comments__comment">${arr[i].body}</div>
            <div class="comment__answer-box">
                <a href="#" class="comments__answer">Відповісти</a>
            </div>
        </div>`;

        comments.insertAdjacentHTML("beforeend", com);
    };
};

function showPaggination(ar, num) {

    loaderOn();

    let temp = num;

    const pagination = document.getElementById("pagination");

    pagination.innerHTML = "";

    let len = Math.ceil(ar.length / comCount);

    if (num < 3) {
        num = 3;
    } else if (num > len - 2) {
        num = len - 2;
    };

    let begin = num - 2;
    let end = num + 2;


    if (begin > 1) {
        pag(`<span class="pagination__item">1</span>`);
    };
    if (temp > 1) {
        pag(`<span class="pagination__item"><<</span>`);
    };

    for (i = begin; i <= end; i++) {
        pag(`<span class="pagination__item">${i}</span>`);
    };

    if (temp < len) {
        pag(`<span class="pagination__item">>></span>`);
    };
    if (end < len) {
        pag(`<span class="pagination__item">${len}</span>`);
    };

    addEventPag(temp);

    loaderOff();

    function pag(str) {
        pagination.insertAdjacentHTML("beforeend", str);
    };
};

function addEventPag(t) {
    const [...pagItems] = document.getElementsByClassName("pagination__item");

    pagItems.forEach(el => {
        if (el.innerHTML == t) {
            el.classList.add("pag-active");
        };

        el.addEventListener("click", () => {
            loaderOn();

            if (el.innerText === "<<") {
                showComment(t - 1);
                showPaggination(arr, t - 1);
            } else if (el.innerText === ">>") {
                showComment(t + 1);
                showPaggination(arr, t + 1);
            } else {
                showComment(parseInt(el.innerText));
                showPaggination(arr, parseInt(el.innerText));
            };

            loaderOff();
        });
    });
};

function loaderOn() {
    loader.classList.add("enable");
    loader.classList.remove("disable");
};

function loaderOff() {
    loader.classList.add("disable");
    loader.classList.remove("enable");
};